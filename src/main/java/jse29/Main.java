package jse29;

import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import static jse29.TerminalConst.*;

public class Main {
    NumberService numberService = new NumberService();

    NumberController numberController = new NumberController(numberService);

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Main app = new Main();
        String command;
        app.numberController.displayWelcome();
        while (!EXIT.equals(command = scanner.nextLine())) {
            app.run(command);
        }

    }
    /**
     * Выполнение комманды
     * @param param Команда
     */
    public void run(final String param) {
        try {
            if (param == null || param.isEmpty()) {
                throw new IllegalArgumentException("Не задана комманда");
            }
            switch (param) {
                case SUM: numberController.calcSum();break;
                case FACTORIAL: numberController.calcFactorial();break;
                case FIBONACCI: numberController.decomposeFibonacci();break;
                case HELP: numberController.displayHelp();break;
                default: throw new IllegalArgumentException("Неизвестная комманда "+param);
            }
        } catch (IllegalArgumentException | InterruptedException | ExecutionException e) {
            System.out.println(e.getMessage());
        }
    }

}
